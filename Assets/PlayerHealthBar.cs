﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerHealthBar : MonoBehaviour
{
    public GameObject Player;
    private PlayerController playerController;

    public Image HealthBar;

    private RectTransform rectTransform;

    private float rectTransformPosX;

    private float startingHealth;

    private void Awake() {
        rectTransform = GetComponent<RectTransform>();
        playerController = Player.GetComponent<PlayerController>();
        rectTransformPosX = rectTransform.rect.x;
    }
    void Start()
    {
        startingHealth = playerController.startingHealth;
    }

    void Update()
    {
        rectTransform.anchoredPosition = new Vector2(Player.transform.position.x + rectTransformPosX + rectTransform.rect.width/2, Player.transform.position.y + .15f + rectTransform.rect.height/2);
        HealthBar.fillAmount = PlayerController.CurrentHealth / startingHealth;
        if(PlayerController.CurrentHealth <= 0) {
            gameObject.SetActive(false);
        }
    }
}
