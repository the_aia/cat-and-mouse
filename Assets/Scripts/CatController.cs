﻿using UnityEngine;

public class CatController : MonoBehaviour{

    public float StartingSpeed;

    private Rigidbody2D thisRigidbody2D;
    private GameObject player;

    private Vector2 randomPosition;

    private bool moveTowardsPlayer;
    private bool newPositionSet = false;

    private float randomMoveTimer;

    public float StartingHealth;
    [HideInInspector]
    public float CurrentHealth;
    public float HitStrength;

    private void Awake() {
        thisRigidbody2D = GetComponent<Rigidbody2D>();
        player = GameObject.FindGameObjectWithTag("Player");
    }

    void Start()
    {
        while(randomPosition == null || Vector2.Distance(player.transform.position, randomPosition) < 3f) {
            SetRandomPosition();
            transform.position = randomPosition;
        }
    }

    // Update is called once per frame
    void Update()
    {
        float distanceToPlayer = Vector2.Distance(player.transform.position, transform.position);
        if(distanceToPlayer < 2f){
            moveTowardsPlayer = true;
            StartingSpeed = 55f;
        } else {
            moveTowardsPlayer = false;
            StartingSpeed = 40f;
        }

        if(Input.GetKeyDown(KeyCode.F2)) {
            Debug.Log("CAT Velocity: " + thisRigidbody2D.velocity);
            Debug.Log("CAT Speed: " + StartingSpeed);
            Debug.Log("Random Move Timer: " + randomMoveTimer);
        }

        if(randomMoveTimer > 0) {
            randomMoveTimer = Mathf.Max(0, randomMoveTimer - Time.deltaTime);
        }
    }
    private void FixedUpdate() {
        if(!moveTowardsPlayer || !player.activeSelf) {
            if(!newPositionSet) {
                SetRandomPosition();
            }

            if(Vector2.Distance(transform.position, randomPosition) > 1f || randomMoveTimer != 0) {
                thisRigidbody2D.velocity = randomPosition.normalized * StartingSpeed * Time.fixedDeltaTime;
            } else {
                newPositionSet = false;
            }
        } else {
            //When near player, chase them
            Vector2 targetLocation = player.transform.position - transform.position;
            thisRigidbody2D.velocity = targetLocation.normalized * StartingSpeed * Time.fixedDeltaTime;
        }
    }

    private void SetRandomPosition(){
        randomPosition = new Vector2(Random.Range(-7f, 7f), Random.Range(-4.3f, 4.3f));
        newPositionSet = true;
        randomMoveTimer = 5f;
    }


    private void OnCollisionEnter2D(Collision2D collision) {
        if(!collision.gameObject.CompareTag("Player")) {
            SetRandomPosition();
        }
    }

    private void OnCollisionStay2D(Collision2D collision) {
        OnCollisionEnter2D(collision);
    }
}
