﻿using UnityEngine;
using UnityEngine.Tilemaps;

public class PlayerController : MonoBehaviour
{
    public float Speed;

    private Rigidbody2D thisRigidbody2D;

    public GameObject PlayerPointer;

    public Grid Grid;
    public Tilemap colliderTilemap;

    public Tile[] playerTiles;

    Vector3 mouse_pos;
    Vector3 object_pos;
    float angle;

    float moveX;
    float moveY;
    Vector2 movement;
    Vector2 hurtForce;

    bool isHurt = false;

    [HideInInspector]
    public static float CurrentHealth;
    public float startingHealth;

    private void Awake() {
        thisRigidbody2D = GetComponent<Rigidbody2D>();

    }

    private void Start() {
        CurrentHealth = startingHealth;
    }

    private void  Update() {
        mouse_pos = Input.mousePosition;
        mouse_pos.z = 5.23f; //The distance between the camera and object
        object_pos = Camera.main.WorldToScreenPoint(transform.position);
        mouse_pos.x = mouse_pos.x - object_pos.x;
        mouse_pos.y = mouse_pos.y - object_pos.y;
        angle = Mathf.Atan2(mouse_pos.y, mouse_pos.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle));

        if(Input.GetMouseButtonDown(0)) {
            Debug.Log("Left Mouse pressed");
            Vector3Int playerPointerPosition = Grid.WorldToCell(PlayerPointer.transform.position);
            Debug.Log("Poointer Position: " +  playerPointerPosition);
            colliderTilemap.SetTile(playerPointerPosition, playerTiles[1]);
        }

        moveX = Input.GetAxisRaw("Horizontal");
        moveY = Input.GetAxisRaw("Vertical");
        movement = new Vector2(moveX, moveY).normalized;

        if(CurrentHealth <= 0) {
            gameObject.SetActive(false);
        }
    }
    void FixedUpdate()
    {
        thisRigidbody2D.velocity = movement * Speed * Time.fixedDeltaTime;

        if(Input.GetKeyDown(KeyCode.F2)) {
            Debug.Log("Horizontal Input: " + Input.GetAxisRaw("Horizontal") + ", Speed: " + Speed);
        }

        if(isHurt) {
            thisRigidbody2D.AddForce(hurtForce, ForceMode2D.Impulse);
            isHurt = false;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        if(collision.gameObject.CompareTag("Enemy")) {
            CurrentHealth -= collision.gameObject.GetComponent<CatController>().HitStrength;
        }

        if(collision.gameObject.CompareTag("Hurt")) {
            var collisionPoint = collision.GetContact(0).point - collision.GetContact(0).normal * .2f;
            //Debug.DrawLine(collision.GetContact(0).point, collision.GetContact(0).point - collision.GetContact(0).normal * .2f, Color.black, 2f);
            //Debug.Log("Collision point: " + collision.GetContact(0).point);
            Vector3Int gridCollisionPoint = Grid.WorldToCell(collisionPoint);
            //Debug.Log("Grid Collision point: " + gridCollisionPoint);
            string objectHit = collision.gameObject.GetComponent<Tilemap>().GetTile(gridCollisionPoint).name;
            Debug.Log(objectHit + " hit");
            switch(objectHit)
            {
                case "cactus":
                    hurtForce = -collisionPoint.normalized * 10f;
                    CurrentHealth -= .5f;
                    isHurt = true;
                    break;
            }
        }
    }
}
